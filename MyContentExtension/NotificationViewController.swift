//
//  NotificationViewController.swift
//  MyContentExtension
//
//  Created by iulian david on 11/24/16.
//  Copyright © 2016 iulian david. All rights reserved.
//

import UIKit
import UserNotifications
import UserNotificationsUI

class NotificationViewController: UIViewController, UNNotificationContentExtension {

   
    @IBOutlet weak var image: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any required interface initialization here.
    }
    
    /**
     Read the notification received and show the attach that came with it
    */
    func didReceive(_ notification: UNNotification) {
        guard  let attachment = notification.request.content.attachments.first else {
            return
        }
        
        //Because the notification is outside the application sandbox we need to access it accession security scoped resource
        if attachment.url.startAccessingSecurityScopedResource(){
            if let imageData = try? Data.init(contentsOf : attachment.url) {
            image.image = UIImage(data: imageData)
            }
        }
    }
    
    /**
     Implement an action on notification
     The action will be sent back to Notification App via a Notification Category
    */
    func didReceive(_ response: UNNotificationResponse, completionHandler completion: @escaping (UNNotificationContentExtensionResponseOption) -> Void) {
        if response.actionIdentifier == "ok" {
            completion(.dismissAndForwardAction)
        } else if response.actionIdentifier == "cancel" {
             completion(.dismissAndForwardAction)
        }
    }

}
