//
//  AppDelegate.swift
//  NotificationsApp
//
//  Created by iulian david on 11/23/16.
//  Copyright © 2016 iulian david. All rights reserved.
//

import UIKit
import UserNotifications

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?


    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        //Add the AppDelegat as the delegate to UserNotification center 
        UNUserNotificationCenter.current().delegate = self
        
        configureUserNotifications()
        return true
    }


    private func configureUserNotifications(){
        //Adding actions
        let okAction = UNNotificationAction(identifier: "ok", title: "😈 Ok", options: [])
        let cancelAction = UNNotificationAction(identifier: "cancel", title: "😡 Dismiss", options: [])
        
        let category = UNNotificationCategory(identifier: notificationCategory, actions: [okAction, cancelAction], intentIdentifiers: [], options: [])
        UNUserNotificationCenter.current().setNotificationCategories([category])
    }
}

/**
 Extend AppDelegate with UserNotificationCenter methods
 */
extension AppDelegate: UNUserNotificationCenterDelegate{
    //we need to set a method when the app is going to the foreground
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        completionHandler(.alert)
    }
    
    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
        print("Response received for \(response.actionIdentifier)")
        completionHandler()
    }
}

